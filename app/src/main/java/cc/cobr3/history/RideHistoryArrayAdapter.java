package cc.cobr3.history;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import cc.cobr3.R;

public class RideHistoryArrayAdapter extends ArrayAdapter<RideHistory> {

    private final Context context;
    private final List<RideHistory> rides;

    public RideHistoryArrayAdapter(@NonNull Context context, int resource, @NonNull List<RideHistory> rides) {
        super(context, resource, rides);
        this.context = context;
        this.rides = rides;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.history_entry, null);

        TextView textView = view.findViewById(R.id.history_entry_date);
        textView.setText(this.rides.get(position).getDate());

        return view;
    }
}
