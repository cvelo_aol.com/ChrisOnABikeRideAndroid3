package cc.cobr3.history;

public class RideHistory {

    private final int id;
    private final String date;
    private final String startTime;
    private final String endTime;
    private final boolean nextDay;


    public RideHistory(int id, String date, String startTime, String endTime, boolean nextDay) {
        this.id = id;
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.nextDay = nextDay;
    }

    public int getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public boolean isNextDay() {
        return nextDay;
    }
}
