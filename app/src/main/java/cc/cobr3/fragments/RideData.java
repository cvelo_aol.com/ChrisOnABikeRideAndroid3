package cc.cobr3.fragments;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.Observable;
import androidx.databinding.ObservableField;
import androidx.databinding.library.baseAdapters.BR;

public class RideData extends BaseObservable {

    private ObservableField<String> test = new ObservableField<>("Yo");

    public ObservableField<String> getTest() {
        return test;
    }

    public void setTest(ObservableField<String> test) {
        this.test = test;
    }
}
