package cc.cobr3.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.ListFragment;

import java.util.List;

import cc.cobr3.R;
import cc.cobr3.activity.TitleBar;
import cc.cobr3.history.RideHistory;
import cc.cobr3.history.RideHistoryArrayAdapter;

public class HistoryFragment extends ListFragment {

    private static final String TAG = "HistoryFragment";
    private TitleBar titleBar;

    public HistoryFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        RideHistory ri1 = new RideHistory(1, "10/11/2021", "6:30 AM", "7:00 AM", false);
        RideHistory ri2 = new RideHistory(1, "10/11/2021", "5:00 PM", "5:30 AM", false);
        RideHistory ri3 = new RideHistory(1, "10/12/2021", "6:23 AM", "6:51 AM", false);

        RideHistoryArrayAdapter rideHistoryArrayAdapter = new RideHistoryArrayAdapter(getActivity(), R.layout.history_entry, List.of(ri1, ri2, ri3));
        setListAdapter(rideHistoryArrayAdapter);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        Log.d(TAG, "onAttach");
        super.onAttach(context);
        TitleBar titleBar = (TitleBar) context;
        this.titleBar = titleBar;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        titleBar.updateTitle(R.string.history_title);
    }
}
