package cc.cobr3.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;

import java.util.concurrent.TimeUnit;

import cc.cobr3.R;
import cc.cobr3.activity.TitleBar;
import cc.cobr3.databinding.FragmentRideBinding;

public class RideFragment extends Fragment {

    private static final String TAG = RideFragment.class.getSimpleName();
    private TitleBar titleBar;
    private RideData rideData;

    public RideFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        FragmentRideBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_ride, container, false);
        rideData = new RideData();
        binding.setData(rideData);


        Runnable r = () -> {
            try {
                Thread.sleep(TimeUnit.SECONDS.toMillis(10));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "onCreateView: setFromThread");
            rideData.getTest().set("Set From thread!");
        };
        new Thread(r).start();

        return binding.getRoot();

    }

    @Override
    public void onAttach(@NonNull Context context) {
        Log.d(TAG, "onAttach");
        super.onAttach(context);
        TitleBar titleBar = (TitleBar) context;
        this.titleBar = titleBar;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onViewCreated: start");
        super.onViewCreated(view, savedInstanceState);
        titleBar.updateTitle(R.string.ride_title);
    }

}
