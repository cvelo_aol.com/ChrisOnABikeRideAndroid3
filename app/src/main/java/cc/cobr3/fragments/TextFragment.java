package cc.cobr3.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import cc.cobr3.R;
import cc.cobr3.activity.TitleBar;

public class TextFragment extends Fragment {

    private static final String TAG = TextFragment.class.getSimpleName();
    private TitleBar titleBar;

    public TextFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_text, container, false);
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        Log.d(TAG, "onAttach");
        super.onAttach(context);
        TitleBar titleBar = (TitleBar) context;
        this.titleBar = titleBar;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onViewCreated: start");
        super.onViewCreated(view, savedInstanceState);
        titleBar.updateTitle(R.string.text_title);
    }
}
