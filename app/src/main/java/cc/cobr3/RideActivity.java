package cc.cobr3;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.appbar.MaterialToolbar;

import java.util.List;

import cc.cobr3.activity.TitleBar;
import cc.cobr3.fragments.HistoryFragment;
import cc.cobr3.fragments.RideFragment;
import cc.cobr3.fragments.TextFragment;

public class RideActivity extends AppCompatActivity implements View.OnClickListener, TitleBar {

    private static final String TAG = RideActivity.class.getSimpleName();

    private MaterialToolbar toolbar;
    private int currentTitle;
    private TheViewModel viewModel;

   public static class TheViewModel extends ViewModel {
        public int titleId;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");

        viewModel = new ViewModelProvider(this).get(TheViewModel.class);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.ride_main);

        toolbar = findViewById(R.id.topAppBar);

        FragmentManager fragmentManager = getSupportFragmentManager();

        Fragment fragment = fragmentManager.findFragmentById(R.id.main_fragment);
        if (null == fragment) {
            fragment = new RideFragment();
            fragmentManager.beginTransaction().add(R.id.main_fragment, fragment).commit();
        }

        setupMenuClicks();

        updateUi();
    }

    @Override
    public void onClick(View v) {
        Class<? extends Fragment> fragmentClass;

        switch (v.getId()) {
            case R.id.menu_button_ride:
                Log.d(TAG, "MenuNav: ride");
                fragmentClass = RideFragment.class;
                break;
            case R.id.menu_button_ride_history:
                Log.d(TAG, "MenuNav: history");
                fragmentClass = HistoryFragment.class;
                break;
            case R.id.menu_button_texts:
                Log.d(TAG, "MenuNav: texts");
                fragmentClass = TextFragment.class;
                break;
            default:
                Log.e(TAG, "no handler for " + v.getId());
                fragmentClass = null;
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.main_fragment, fragmentClass, null)
                .setReorderingAllowed(true)
                .addToBackStack(null)
                .commit();

    }

    private void setupMenuClicks() {

        List.of(R.id.menu_button_ride, R.id.menu_button_ride_history, R.id.menu_button_texts)
                .stream()
                .map(this::findViewById)
                .forEach(e -> ((View) e).setOnClickListener(RideActivity.this));

    }

    public void updateTitle(int id) {
        viewModel.titleId = id;
        updateUi();
    }

    public void updateUi() {
        Log.d(TAG, "updateUi: viewModel.titleId: " + viewModel.titleId);
        viewModel.titleId  = 0 == viewModel.titleId ? R.string.ride_title : viewModel.titleId;
        Log.d(TAG, "updateUi: toolbar: " + this.toolbar);
        this.toolbar.setTitle(viewModel.titleId);
    }


}